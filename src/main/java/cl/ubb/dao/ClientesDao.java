package cl.ubb.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import cl.ubb.modelos.*;

public interface ClientesDao extends CrudRepository<Cliente, Long>{
	
	public List<Cliente> findClientes(String rut, String nombre, String numeroTelefono);
}
