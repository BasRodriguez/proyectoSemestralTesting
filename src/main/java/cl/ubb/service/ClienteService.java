package cl.ubb.service;

import cl.ubb.dao.*;
import cl.ubb.modelos.*;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


public class ClienteService {

	@Autowired
	private ClientesDao dao;
	
	public Cliente registrarCliente(Cliente cliente) {
		return dao.save(cliente);
	}

	public List<Cliente> obtenerClientes() {
		List<Cliente> misClientes = new ArrayList<Cliente>();
		Cliente cliente = new Cliente();
		
		String rut=cliente.getRut();
		String nombre=cliente.getNombre();
		String numeroTelefono=cliente.getCelular();
		
		misClientes= (ArrayList<Cliente>) dao.findClientes(rut,nombre,numeroTelefono);
		
		return misClientes;
		
	}

}
