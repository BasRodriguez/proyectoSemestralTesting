package cl.ubb.service;

import  org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.*;
import cl.ubb.modelos.*;
import cl.ubb.service.*;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

public class ClienteServiceTest {
	
	@Mock
	private ClientesDao clientedao;
	
	@InjectMocks
	public static ClienteService clienteService;
	
	@Test
	public void RegistrarUnCliente(){
		Cliente cliente=new Cliente();
		Cliente clienteRetornado=new Cliente();
		
		when(clientedao.save(cliente)).thenReturn(cliente);
		clienteRetornado= clienteService.registrarCliente(cliente);
		
		Assert.assertNotNull(clienteRetornado);
		Assert.assertEquals(clienteRetornado, cliente);
	}
	
	@Test
	public void ListarTodosLosClientes(){
		List<Cliente> misClientes = new ArrayList<Cliente>();
		List<Cliente> resultado = new ArrayList<Cliente>();
		
		when(clientedao.findAll()).thenReturn(misClientes);
		resultado = clienteService.obtenerClientes();
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(resultado, misClientes);
		
		
		
		
	}
	
	
}
